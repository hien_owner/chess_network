/*
 C ECHO client example using sockets
 */
#include<stdio.h> //printf
#include<string.h>    //strlen
#include<sys/socket.h>    //socket
#include<arpa/inet.h> //inet_addr
#include"src/protocol.h"

int room_id = -1;
char currentCommand[20];
char playerid[20];

int main(int argc , char *argv[])
{
    int sock;
    struct sockaddr_in server;
    char message[1000] , server_reply[2000];
    int read_size;
    char temp[1000];
    
    char command[20];
    char *token;
    
    if (argc !=2) {
        perror("Usage: TCPClient <idname>"); 
        exit(1);
    }
    strcpy(playerid, argv[1]);
    //Create socket
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");
    
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_family = AF_INET;
    server.sin_port = htons( 8888 );
    
    //Connect to remote server
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        perror("connect failed. Error");
        return 1;
    }
    
    puts("Connected\n");
    
    //keep communicating with server
    while(1)
    {
        printf("Enter message : ");
        //scanf("%s" , message);
        //fgets (message, 100, stdin);
        gets(message);
        strcpy(temp, message);
        token = strtok(temp, " ");
        if(token != NULL){
            strcpy(command, token);
        }
        //puts(message);
        //printf("command: %s", command);
        // if(checkCommand(command) == 1){
        //Send some data
        if( send(sock , message , strlen(message) , 0) < 0)
        {
            puts("Send failed");
            return 1;
        }
        
        //Receive a reply from the server
        if(( read_size = recv(sock , server_reply , 2000 , 0)) < 0)
        {
            puts("recv failed");
            break;
        }
        server_reply[read_size] = '\0';
        puts("Server reply :");
        puts(server_reply);
        puts("___________");
        // }else{
        //     puts("wrong command");
        // }
    }
    
    close(sock);
    return 0;
}

int checkCommand(char requestCommand[100]){
    //if command is FIND and user already have room, return false
    if(strcmp(requestCommand, "FIND") == 0){
        if(room_id != -1){
            return 0;
        }
    } else{
        strcpy(currentCommand, requestCommand);
        return 1;
    }
    return 0;
}
