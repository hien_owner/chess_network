/*
 C socket server example, handles multiple clients using threads
 */

#include<stdio.h>
#include<string.h>    //strlen
#include<stdlib.h>    //strlen
#include<sys/socket.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include<pthread.h> //for threading , link with lpthread

#include"src/protocol.h"

//the thread function
void *connection_handler(void *);

/**
 * we have 100 room
 *  player1 : unique id of player 1
 *   player2 : unique id of player 2
 *  status 0~> empty, 1~> 1 player wait ,    2 ~> playing
 * current turn =1 or = 2
 */
struct Room
{
    char playerid1[50];
    char playerid2[50];
    int playersock1;
    int playersock2;
    int status;
    int currentTurn;
    char lastMove[100];
} rooms[MAXROOM]; 

int connect_num = 0;
char responseMessage[20];

int socket_desc , client_sock , c , *new_sock;
struct sockaddr_in server , client;
int command;

char serverDebug[1000];

int main(int argc , char *argv[])
{
    int i;
    
    puts("Welcome to ICT chess server");
    puts("Server starting...");
    
    for(int i=0;i<MAXROOM; i++){
        rooms[i].status = 0;
        rooms[i].currentTurn = 0;
    }
    
    //Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");
    
    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( PORT_SERVER );
    
    //Bind
    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        //print the error message
        perror("bind failed. Error");
        return 1;
    }
    puts("bind done");
    
    //Listen
    listen(socket_desc , 3);
    
    //Accept and incoming connection
    puts("Server ready");
    puts("Waiting for incoming connections...");
    c = sizeof(struct sockaddr_in);
    
    
    //    //Accept and incoming connection
    //    puts("Waiting for incoming connections...");
    //    
    //    c = sizeof(struct sockaddr_in);
    while( (client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c)) )
    {
        sprintf(serverDebug, "%s %d", "Client connected id: ",client_sock);
        puts(serverDebug);
        
        pthread_t sniffer_thread;
        new_sock = malloc(1);
        *new_sock = client_sock;
        
        if( pthread_create( &sniffer_thread , NULL ,  connection_handler , (void*) new_sock) < 0)
        {
            perror("could not create thread");
            return 1;
        }
        
        //Now join the thread , so that we dont terminate before the thread
        //pthread_join( sniffer_thread , NULL);
        //puts("Handler assigned");
    }
    
    if (client_sock < 0)
    {
        perror("accept failed");
        return 1;
    }
    
    return 0
            ;}

/*
 * This will handle connection for each client
 * */
void *connection_handler(void *socket_desc)
{
    //Get the socket descriptor
    int sock = *(int*)socket_desc;
    int read_size;
    char *message , client_message[2000];
    int i;
    // for(i=0;i<strlen(client_message); i++){
    //     client_message[i]="\0";
    // }
    //Send some messages to the client
    // message = "Greetings! I am your connection handler\n";
    // write(sock , message , strlen(message));
    
    // message = "Now type something and i shall repeat what you type \n";
    // write(sock , message , strlen(message));
    
    //Receive a message from client
    while( (read_size = recv(sock , client_message , 2000 , 0)) > 0 )
    {
        //Send the message back to client
        //printf("%d", read_size);
        client_message[read_size] = '\0';
        sprintf(serverDebug, "%s %d %s %s ", "Client id",sock,"send command:", client_message);
        puts(serverDebug);
        
        handleRequest(sock, client_message);
        
        connect_num++;
        //sprintf(client_message, "%d", connect_num);
        
        //puts(client_message);
        //write(sock , client_message , strlen(client_message));
        write(sock , responseMessage , strlen(responseMessage));
    }
    
    if(read_size == 0)
    {
        puts("Client disconnected");
        fflush(stdout);
    }
    else if(read_size == -1)
    {
        perror("recv failed");
    }
    
    //Free the socket pointer
    free(socket_desc);
    
    return 0;
}

int handleRequest(int sock, char request[100]){
    char command[20];
    char param0[20];
    char param1[20];
    char param2[20];
    char *token;
    int action_response;
    int room_id;
    
    token = strtok(request, " ");
    if(token != NULL){
        strcpy(command, token);
        token = strtok(NULL, " ");
    }
    if(token != NULL){
        strcpy(param0, token);
        token = strtok(NULL, " ");
    }
    if(token != NULL){
        strcpy(param1, token);
        token = strtok(NULL, " ");
    }
    if(token != NULL){
        strcpy(param2, token);
        token = strtok(NULL, " ");
    }
    
    //if user select find room
    /*
     find room command
     FIND <playerid>
     server return room_id or -1(error)
     */
    if(strcmp(command, "FIND") == 0){
        //puts("find action");
        action_response = findRoom(sock, param0);
        if(action_response>=0){
            room_id = action_response;
            sprintf(responseMessage, "%s %d %d %d", "FIND_RESPONSE", RESPONSE_SUCCESS,action_response, rooms[room_id].status);
            return 1;
        }else{
            sprintf(responseMessage, "%s %d %s", "FIND_RESPONSE", RESPONSE_ERROR, action_response);
            return 1;
        }
    }
    
    /*
     move command
     MOVE <roomid> <playerid> <movecode>
     
     */
    if(strcmp(command, "MOVE") == 0){
        room_id = atoi(param0); 
        action_response = move(sock, room_id, param1, param2);
        if(action_response == MOVE_VALID){
            sprintf(responseMessage, "%s %d %d", "MOVE_RESPONSE", RESPONSE_SUCCESS, action_response);
        }else{
            sprintf(responseMessage, "%s %d %d", "MOVE_RESPONSE", RESPONSE_ERROR, action_response);
        }
        return 1;
    }
    
    /*
     quit command
     QUIT <roomid> <playerid>
     
     */
    if(strcmp(command, "QUIT") == 0){
        room_id = atoi(param0);
        action_response = quit(sock, room_id, param1);
        sprintf(responseMessage, "%s %d", "QUIT_RESPONSE", action_response);
        return 1;
    }
    
    /*
     admin command
     */
    if(strcmp(command, "ADMIN") == 0){
        action_response = admin(param0, param1);
        return 1;
    }
    sprintf(responseMessage, "%d %s", RESPONSE_ERROR,"UNKNOWN_REQUEST");
    return 1;
}

int findRoom(int sock, char playerid[20]){
    int i;
    int roomNum = -1;
    if(isUserExist(playerid)){
        return -2;
    }
    for(int i=0;i<MAXROOM;i++){
        if(rooms[i].status < 2){
            roomNum = i;
            if(rooms[i].status == 0){
                strcpy(rooms[i].playerid1, playerid);
                rooms[i].playersock1 = sock;
            }
            if(rooms[i].status == 1){
                strcpy(rooms[i].playerid2, playerid);
                rooms[i].playersock2 = sock;
            }
            rooms[i].status ++;
            if(rooms[i].status == 2){
                roomReady(i);
            }
            break;
        }
    }
    return roomNum;
}

int roomReady(int roomId){
    rooms[roomId].currentTurn = 1;
}

int isUserExist(char playerid[20]){
    int i;
    for(int i=0;i<MAXROOM;i++){
        if(rooms[i].status != 0){
            if(strcmp(playerid, rooms[i].playerid1) == 0){
                return 1;
            }
            if(strcmp(playerid, rooms[i].playerid2) == 0){
                return 1;
            }
        }
    }
    return 0;
}

int move(int sock, int roomid, char playerid[20], char movecode[20]){
    int i;
    int playerNum = 0;
    
    if((roomid<0)||(roomid>MAXROOM)){
        return MOVE_ROOM_NOT_FOUND;
    }
    
    if(rooms[roomid].status!=2){
        
        return MOVE_ROOM_NOT_READY;   
    }else{
        
        if(strcmp(rooms[roomid].playerid1, playerid) == 0){
            playerNum = 1;
            if(sock != rooms[roomid].playersock1){
                return MOVE_ILEGAL;
            }
        }
        if(strcmp(rooms[roomid].playerid2, playerid) == 0){
            playerNum = 2;
            if(sock != rooms[roomid].playersock2){
                return MOVE_ILEGAL;
            }
        }
        if(playerNum == 0){
            return MOVE_ILEGAL;
        }else{
            if(playerNum != rooms[roomid].currentTurn){
                return MOVE_ILEGAL;
            }else{
                if (checkValidMove(roomid, playerid, movecode) == MOVE_VALID){
                    //move chess
                    strcpy(rooms[roomid].lastMove, movecode);
                    responseToPlayer(roomid);
                    if(rooms[roomid].currentTurn == 1){
                        rooms[roomid].currentTurn = 2;
                    }else{
                        rooms[roomid].currentTurn = 1;
                    }
                    
                    
                    return MOVE_VALID;
                }else{
                    return MOVE_ILEGAL;
                }
            }
        }
    }
    return MOVE_ILEGAL;
}

int checkValidMove(int roomid, char playerid[20], char movecode[20]){
    return MOVE_VALID;
}

void responseToPlayer(int roomid){
    char response_move[100];
    
    sprintf(response_move, "%s %d %s \n","MOVE_CREATE_RESPONSE",rooms[roomid].currentTurn, rooms[roomid].lastMove);
    puts(response_move);
    write(rooms[roomid].playersock1 , response_move , strlen(response_move));
    write(rooms[roomid].playersock2 , response_move , strlen(response_move));
}

int quit(int sock, int roomid, char playerid[20]){
    if((strcmp(rooms[roomid].playerid1, playerid) == 0) && (strcmp(rooms[roomid].playersock1, sock) == 0)){
        strcpy(rooms[roomid].playerid1, "\0");
        rooms[roomid].playersock1 = 0;
        rooms[roomid].status--;
        strcpy(rooms[roomid].lastMove, "\0");
        rooms[roomid].currentTurn = 1;
        return RESPONSE_SUCCESS;
    }
    if((strcmp(rooms[roomid].playerid2, playerid) == 0) && (strcmp(rooms[roomid].playersock2, sock) == 0)){
        strcpy(rooms[roomid].playerid2, "\0");
        rooms[roomid].playersock2 = 0;
        rooms[roomid].status--;
        strcpy(rooms[roomid].lastMove, "\0");
        rooms[roomid].currentTurn = 1;
        return RESPONSE_SUCCESS;
    }
    return RESPONSE_ERROR;
}

int admin(char pass[20], char command[20]){
    if(strcmp(pass, ADMIN_PASS) == 0){
        sprintf(responseMessage, "%s %s", "AMDIN_RESPONSE", "SUCCESS");
    }else{
        sprintf(responseMessage, "%s %s", "AMDIN_RESPONSE", "INVALID PASSWORD");
    }
}