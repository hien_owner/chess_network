/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   protocol.h
 * Author: hiennq2
 *
 * Created on May 15, 2017, 3:53 PM
 */

#ifndef PROTOCOL_H
#define PROTOCOL_H

//max room 
#define MAXROOM 100 

//server information
#define IP_SERVER "127.0.0.1"
#define PORT_SERVER 8888

#define ADMIN_PASS "123456"

//response code
#define RESPONSE_ERROR 0
#define RESPONSE_SUCCESS 1

//find room action
#define FIND_ROOM_FAIL -1
#define FIND_ROOM_PLAYER_EXIST -2
#define FIND_ROOM_SUCCESS 1

//move action
#define MOVE_ROOM_NOT_FOUND -1
#define MOVE_ILEGAL -3
#define MOVE_ROOM_NOT_READY -2
#define MOVE_VALID 1

extern char clientName[100];
extern char ipServer[100];
extern int portServer;

/**
 * Protocol command
 * find game:               FIND <playerid>
 * find game server response:  FIND_RESPONSE <responsecode> <roomid> <status>
 *                            status = 1 => you are player1, status =2 -> you are player 2 
 * move a chess:            MOVE <roomid> <playerid> <movecode>
 * server response move to 2 player:    MOVE_CREATE_RESPONSE <playerTurn> <playerMove>
 * server response for client move:   MOVE_RESPONSE <error> <movestatus>
 *  quit game               QUIT <roomid> <playerid>
 * server response for client   QUIT_RESPONSE <error_code>
 *  
 **/

/**
 * admin command       
 * player online    ADMIN <adminpass> PLAYER_LIST
 * room active      ADMIN <adminpass> ROOM_LIST
 
 **/

#endif /* PROTOCOL_H */

