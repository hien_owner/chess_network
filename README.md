Check game with TCP network protocol
==========

A chess board. Designed to be simple, fast, and look nice.
Multiplayer , find match, support both command line and GUI

##install
    - Ragel: sudo apt install ragel
    - librsvg: sudo apt install librsvg2-dev
    - gtk-3 :  sudo apt install libgtk-3-dev

##Makefile
-   make clean && make
-   gcc -pthread -o server.o server.c
-   gcc -o client.o client.c

##Run server
./server.o
##Run terminal client
./client.o

##Run
chmod a+x chessboard
./chessboard